import LightningDatatable from 'lightning/datatable';
import customCmpToInclude from './customCmpToInclude.html';

export default class DataTable extends LightningDatatable {
    // we have to 'customTypes' as object name here otherwise it will not work
    static customTypes = {
        // the object label here must be same as the type you have defined for the 
        // column( in which your custom cmp will be placed) in mainCmp.js
        // we are using 'custom' as label here as we have defined the column type as 'custom'in our mainCmp.js
        custom: {
            template: customCmpToInclude,
            typeAttributes: ['customAttribute'] // the value inside brackets must be in accordance with typeAttributes property of your custom cmp column in mainCmp.js 
        }
    };
}