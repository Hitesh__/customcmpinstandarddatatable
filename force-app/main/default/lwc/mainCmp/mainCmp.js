import {
    LightningElement,
    track
} from 'lwc';

export default class MainCmp extends LightningElement {

    columns = [{
            label: 'Name',
            fieldName: 'name',
            type: 'text'
        },
        {
            label: 'Custom Component',
            fieldName: 'customCmp',
            type: 'custom',
            typeAttributes: {
                customAttribute: 'Button'
            }
        }
    ];

    @track data = [{
        name: 'Sample Data 1'
    }, {
        name: 'Sample Data 2'
    }];
}